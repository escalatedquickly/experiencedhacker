#define _POSIX_C_SOURCE 200809L
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <utils.h>

#define BUFLEN      256
#define LISTEN_IP   "0.0.0.0"
#define LISTEN_PORT 13337

int conn = -1;
int cli  = -1;

void cleanUp(int);
void answerPhone(void);
void smallTalk(int cli);

int
main(int argc, char *argv[])
{
    signal(SIGTERM, cleanUp);
    answerPhone();
    return 0;
}

void
answerPhone(void) {
    char recvbuf[BUFLEN];
    if((conn = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
        die("`socket` failed with error `%s`\n", strerror(errno));
    struct sockaddr_in addr = {
        .sin_family      = AF_INET,
        .sin_addr.s_addr = inet_addr(LISTEN_IP),
        .sin_port        = htons(LISTEN_PORT)
    };
    if(bind(conn, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        close(conn);
        die("`bind` failed with error `%s`\n", strerror(errno));
    }
    struct sockaddr_in cli_addr = {0};
    socklen_t cli_len = sizeof(struct sockaddr_in);
    listen(conn,0);
    while(1){
        if((cli = accept(conn, (struct sockaddr*)&cli_addr, &cli_len)) == -1) {
            close(conn);
            die("`accept` failed with error: '%s'\n", strerror(errno));
        }
        ok("Someone's calling\n");
        recv(cli, recvbuf, BUFLEN, 0);
        if(strcmp(recvbuf, "hello\n") != 0) {
            close(cli);
            continue;
        }
        ok("Answered call\n");
        smallTalk(cli);
        ok("They hung up\n");
    }
    close(conn);
}

void
smallTalk(int cli) {
    char *sendbuf = NULL;
    size_t size;
    while(1) {
        int nread;
        printf("> ");
        if((nread = getline(&sendbuf, &size, stdin)) == -1)
            die("`getline` failed with error: %s\n", strerror(errno));
        sendbuf[nread-1] = 0;
        send(cli, sendbuf, nread, 0);
        if(strcmp(sendbuf, "exit") == 0) break;
        if(strcmp(sendbuf, "clean") == 0) {printf(""); break;}
        char recvbuf[BUFLEN] = {0};
        nread = 0;
        while((nread = recv(cli, recvbuf, BUFLEN, 0)) > 0) {
            if(recvbuf[nread-1] == '\n') recvbuf[nread-1] = 0;
            if(strcmp(recvbuf, "over~") == 0) break;
            printf("%s\n", recvbuf);
            memset(recvbuf, 0, BUFLEN);
            send(cli, "next~", 5, 0);
        }
    }
    close(cli);
}

void
cleanUp(int signal) {
    close(conn);
    close(cli);
    exit(127 + signal);
}
