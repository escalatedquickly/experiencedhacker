# README

This is an implementation of a basic reverse shell. The project consists of two
parts; `client` and `remote`.

## Security notice

This software is potentially malicious, and I take no responsibility for how it
is used. I wrote it solely for entertainment and educational purposes. This
being said, it is very limited, so if you're looking for a legitimate reverse
shell and this is the best you've found, then knock yourself out skid.

## `remote`

`remote` is phoning home to a hard coded IP address (`192.168.57.1` in this
case, since it happened to be the one of my virtual NIC). Once a connection
is established it supports some basic commands; `rm`, `find` and `read`, which
all take a file path (except `find`, which takes a glob) and are self
explanatory. `exit` is also supported, which simply terminates the connection
and closes down the program.

## `client`

`client` is the "home" that `remote` phones home to. It is very basic, and
basically just reads and writes from and to a socket.
