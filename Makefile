CCC = cc
RCC = x86_64-w64-mingw32-gcc

CCFLAGS = -std=c99 -Werror -Wall
RCFLAGS = -std=c99 -Werror -Wall

CLFLAGS =
RLFLAGS = -lgcc -lodbc32 -lwsock32 -lwinspool -lwinmm -lshell32 -lcomctl32 -lodbc32 -ladvapi32 -lodbc32 -lwsock32 -lopengl32 -lglu32 -lole32 -loleaut32 -luuid

CIFLAGS = -Iinclude
RIFLAGS = -Iinclude

SRC   = src
BUILD = build
CSRC  = $(wildcard $(SRC)/*.c)
COUT  = $(patsubst $(SRC)/%.c,$(BUILD)/%,$(CSRC))

all: $(COUT)

$(BUILD)/client: $(SRC)/client.c
	@mkdir -p build
	$(CCC) $(CCFLAGS) -o $@ $< $(CLFLAGS) $(CIFLAGS)

$(BUILD)/remote: $(SRC)/remote.c
	$(RCC) $(RCFLAGS) -o $@ $< $(RLFLAGS) $(RIFLAGS)

clean:
	rm -rf $(BUILD)

.PHONY: all clean
