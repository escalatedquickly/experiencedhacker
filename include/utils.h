#ifndef UTILS_H
#define UTILS_h

#include <stdio.h>
#include <stdarg.h>
#include <sys/types.h>

void
die(char *msg, ...) {
    char *m = NULL;
    size_t s = 0;
    va_list args;
    va_start(args, msg);
    s = vsnprintf(m, s, msg, args);
    m = malloc(s);
    vsprintf(m, msg, args);
    fprintf(stderr, "[[31m-[00m] %s", m);
    va_end(args);
    exit(-1);
}

void
ok(char *msg, ...) {
    char *m = NULL;
    size_t s = 0;
    va_list args;
    va_start(args, msg);
    s = vsnprintf(m, s, msg, args);
    va_end(args);
    m = malloc(s);
    va_start(args, msg);
    vsprintf(m, msg, args);
    fprintf(stdout, "[[32m+[00m] %s", m);
    va_end(args);
}

#endif
